--------------------------------------------------------------------------------
## Install

All the requirements are listed in the file `environment.yaml`. It can be used to create a conda environement as follows:
```console
conda env create -n batorch -f environment.yaml
```
Then add the package to your `PYTHONPATH` or simply do `pip install -e .`

## Documentation

API reference and a small tutorial can be found here : **[Documentation](https://batorch.readthedocs.io/en/latest/?badge=latest)**