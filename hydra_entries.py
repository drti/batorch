import os
import dotenv
import logging

dotenv.load_dotenv(override=True)
logger = logging.getLogger(__name__)

import numpy as np

import hydra
from hydra.core.hydra_config import HydraConfig
from omegaconf import DictConfig

import torch
from batorch.utils.pipelines import mcmc
from batorch.utils.data import DataModule

from torch.optim.optimizer import Optimizer
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

@hydra.main(config_path="configs/", config_name="config_mcmc.yaml")
def mcmc_pipeline(config: DictConfig):
    """Pipeline for Bayesian regression with SGMCMC methods.
    
    The input configuration file is parsed by hydra and passed to :func:`~batorch.utils.pipelines.mcmc`.

    :param config: Configuration given by a :class:`omegaconf.DictConfig`.
    """
    
    if config.gpu:
        device = torch.device("cuda")
        if torch.cuda.is_available():
            logger.info("Cuda is available!")
        else:
            logger.info("Cuda not available")
    else:
        device = torch.device("cpu")
    
    logger.info(f"Instantiating datamodule <{config.datamodule._target_}>")
    datamodule: DataModule = hydra.utils.instantiate(config.datamodule)

    if "seed" in config:
        torch.manual_seed(config.seed)

    logger.info(f"Instantiating neg. loglikelihood function <{config.negloglikelihood._target_}>")
    negloglikelihood = hydra.utils.instantiate(config.negloglikelihood).to(device)
    
    logger.info(f"Instantiating prior distribution <{config.prior._target_}>")
    prior = hydra.utils.instantiate(config.prior).to(device)
    
    logger.info(f"Instantiating sampler <{config.sampler._target_}>")
    sampler = hydra.utils.instantiate(config.sampler, negloglikelihood=negloglikelihood, neglogprior=prior, dataloader=datamodule.train_dataloader())
    
    if "quantization" in config and config.quantization is not None:
        logger.info(f"Instantiating <{config.quantization._target_}>")
        quantization = hydra.utils.instantiate(config.quantization)
    else:
        quantization = None
    
    params, preds, uq_estimates = mcmc(sampler=sampler, datamodule=datamodule,  
                                       max_burnin_iterations=config.max_burnin_iterations, max_sampling_iterations=config.max_sampling_iterations,
                                       device=device, quantization=quantization)
    
    np.save("quantization_weights_samples.npy", params.cpu().numpy())
    np.save("test_predictions.npy", preds)
    np.save("statistics.npy", uq_estimates)
    
@hydra.main(config_path="configs/", config_name="config_map.yaml")
def map_pipeline(config: DictConfig):
    """Pipeline for maximum a posteriori or maximum likelihood estimation.
    
    The input configuration file is parsed by hydra. The functions :func:`~batorch.utils.pipelines.train_loop` and :func:`batorch.utils.pipelines.test_loop` are used to train the model.

    :param config: Configuration given by a :class:`omegaconf.DictConfig`.
    """
    
    from batorch.utils.pipelines import train_loop, test_loop
    
    return_dict = {}
    return_dict["state_dicts"] = []
    return_dict["test_preds"] = []

    if config.gpu:
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    
    logger.info(f"Instantiating neg. log likelihood <{config.neglikelihood._target_}>")                                           
    loglike: torch.nn.Module = hydra.utils.instantiate(config.neglikelihood)

    logger.info(f"Instantiating datamodule <{config.datamodule._target_}>")
    datamodule: DataModule = hydra.utils.instantiate(config.datamodule)
    logger.info(f"Memory reserved/allocated: {torch.cuda.memory_allocated()} / {torch.cuda.memory_reserved()}")

    if "seed" in config:
        torch.manual_seed(config.seed)
    else:
        torch.manual_seed(torch.seed())

    logger.info(f"Start training!")
    if config.datamodule.kfold_splits==1:
        logger.info(f"Single training loop with fixed train/val datasets")

        logger.info(f"Instantiating network <{config.network._target_}>")
        model: torch.nn.Module = hydra.utils.instantiate(config.network).to(device)
        num_params=0
        for p in model.parameters():
            num_params += p.numel()
        logger.info(f"The model has {num_params} parameters.")

        logger.info(f"Instantiating optimizer <{config.optimizer._target_}>")
        optimizer: Optimizer = hydra.utils.instantiate(config.optimizer, params=model.parameters())

        if config.lr_scheduler is not None:
            logger.info(f"Instantiating learning rate scheduler <{config.lr_scheduler._target_}>")
            scheduler: torch.optim._LRScheduler = hydra.utils.instantiate(config.lr_scheduler, optimizer=optimizer)
        else:
            scheduler = None

        writer = SummaryWriter(log_dir=HydraConfig.get().run.dir)
        output = train_loop(num_epoch=config.num_epochs, model=model,
                    train_loader=datamodule.train_dataloader(), val_loader=datamodule.val_dataloader(),
                    optimizer=optimizer, scheduler=scheduler, loglike=loglike,
                    writer=writer, device=device)
        
        return_dict["state_dicts"].append(model.state_dict())
        
        if "test_dataset" in config.datamodule and config.datamodule.test_dataset is not None:
            logger.info(f"Testing trained model!")
            #datamodule.setup("test")
            output = test_loop(model=model, 
                            test_loader=datamodule.test_dataloader(), 
                            loglike=loglike, 
                            writer=writer, 
                            device=device)
            return_dict["test_preds"].append(output["preds"])
        else:
            logger.info(f"No test dataset available.")        

    elif config.datamodule.kfold_splits>1:
        logger.info(f"KFold training loop with {config.datamodule.kfold_splits} folds")

        cv_train_loss = np.zeros(config.num_epochs)
        cv_val_loss = np.zeros(config.num_epochs)
        writer = SummaryWriter(log_dir=HydraConfig.get().run.dir)
        for fold, (train_loader, val_loader) in enumerate(datamodule.kfold_train_val_dataloaders()):
            logger.info(f"Training with fold {fold} ")

            logger.info(f"Instantiating network <{config.network._target_}>")
            model: torch.nn.Module = hydra.utils.instantiate(config.network).to(device)
            num_params=0
            for p in model.parameters():
                num_params += p.numel()
            logger.info(f"The model has {num_params} parameters.")
            logger.info(f"Memory reserved/allocated: {torch.cuda.memory_allocated()} / {torch.cuda.memory_reserved()}")

            logger.info(f"Instantiating optimizer <{config.optimizer._target_}>")
            optimizer: Optimizer = hydra.utils.instantiate(config.optimizer, params=model.parameters())

            if config.lr_scheduler is not None:
                logger.info(f"Instantiating learning rate scheduler <{config.lr_scheduler._target_}>")
                scheduler: torch.optim._LRScheduler = hydra.utils.instantiate(config.lr_scheduler, optimizer=optimizer)
            else:
                scheduler = None

            output = train_loop(num_epoch=config.num_epochs, model=model,
                        train_loader=train_loader, val_loader=val_loader, 
                        optimizer=optimizer, scheduler=scheduler, loglike=loglike,
                        writer=writer, device=device, fold=fold)
            
            return_dict["state_dicts"].append(model.state_dict())
            
            cv_train_loss+=output["train_epoch_loss"]
            cv_val_loss+=output["val_epoch_loss"]
            
            if "test_dataset" in config.datamodule and config.datamodule.test_dataset is not None:
                logger.info(f"Testing trained model!")
                output = test_loop(model=model, 
                                test_loader=datamodule.test_dataloader(), 
                                loglike=loglike, 
                                writer=writer, 
                                device=device,
                                fold=fold)
                return_dict["test_preds"].append(output["preds"])
            else:
                logger.info(f"No test dataset available.")            
            
        cv_train_loss /= fold+1
        cv_val_loss /= fold+1
        
        for i, (s1, s2) in enumerate(zip(cv_train_loss, cv_val_loss)):
            writer.add_scalars(main_tag="Mean cross val. loss", tag_scalar_dict={"Train": s1, "Val": s2}, global_step=i)
        logger.info(f"Last epoch mean train loss: {cv_train_loss[-1]} | Last epoch mean validation loss: {cv_val_loss[-1]}")
        
    writer.flush()    
    
    if "save_state_dict" in config and config.save_state_dict:
        for i, state_dict in enumerate(return_dict["state_dicts"]):
            torch.save(state_dict, os.path.join(os.getcwd(),"map_state_dict_" + str(i) + ".pt"))
    if "save_test_preds" in config and config.save_test_preds:
        if "test_preds" in return_dict:
            np.save(os.path.join(os.getcwd(),"test_predictions.npy"), return_dict["test_preds"])
    
@hydra.main(config_path="configs/", config_name="config_ensemble.yaml")
def ensemble_pipeline(config: DictConfig):
    """Pipeline for Bayesian regression with ensembles.

    The input configuration file is parsed by hydra. The functions :func:`~batorch.utils.pipelines.train_loop` and :func:`batorch.utils.pipelines.test_loop` are used to train an ensemble of models.

    :param config: Configuration given by a :class:`omegaconf.DictConfig`.
    """    
    
    from batorch.utils.pipelines import train_loop, test_loop
    from batorch.utils.misc import parameters_to_vector

    return_dict = {}
    
    if config.gpu:
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    
    if "seed" in config:
        torch.manual_seed(config.seed)

    logger.info(f"Instantiating neg. log likelihood <{config.neglikelihood._target_}>")                                           
    loglike: torch.nn.Module = hydra.utils.instantiate(config.neglikelihood)

    logger.info(f"Instantiating datamodule <{config.datamodule._target_}>")
    datamodule: DataModule = hydra.utils.instantiate(config.datamodule)
    
    predictions = []
    samples = []
    writer = SummaryWriter(log_dir=HydraConfig.get().run.dir)
    for model_index in tqdm(range(config.number_of_models)):
        
        torch.manual_seed(torch.seed())
        
        model: torch.nn.Module = hydra.utils.instantiate(config.network).to(device)
        optimizer: Optimizer = hydra.utils.instantiate(config.optimizer, params=model.parameters())

        if config.lr_scheduler is not None:
            # logger.info(f"Instantiating learning rate scheduler <{config.lr_scheduler._target_}>")
            scheduler: torch.optim._LRScheduler = hydra.utils.instantiate(config.lr_scheduler, optimizer=optimizer)
        else:
            scheduler = None

        output = train_loop(num_epoch=config.num_epochs, model=model,
                            train_loader=datamodule.train_dataloader(), val_loader=datamodule.val_dataloader(),
                            optimizer=optimizer, scheduler=scheduler, loglike=loglike,
                            writer=None, device=device, fold=model_index)
        
        # return_dict["state_dicts"].append(model.state_dict())
        params = parameters_to_vector(model.parameters(), grad=False, both=False, cpu=False)
        samples.append(params)
        
        if "test_dataset" in config.datamodule and config.datamodule.test_dataset is not None:
            output = test_loop(model=model, 
                            test_loader=datamodule.test_dataloader(), 
                            loglike=loglike, 
                            writer=writer, 
                            device=device)
            # return_dict["test_preds"] = output["preds"]  
            predictions.append(output["preds"])
        else:
            logger.info(f"No test dataset available.")
        
    writer.flush()
    predictions = np.stack(predictions,axis=0)
    samples = torch.stack(samples,dim=0)
        
    return_dict["test_preds"] = predictions
    return_dict["samples"] = samples.detach().cpu().numpy()
        
    if "save_weights" in config and config.save_weights:
        np.save(os.path.join(os.getcwd(),"weights_samples.npy"), return_dict["samples"])

    if "save_test_preds" in config and config.save_test_preds:
        if "test_preds" in return_dict:
            np.save(os.path.join(os.getcwd(),"test_predictions.npy"), return_dict["test_preds"])
    
    logger.info(f"[6] Computing statistics")
    from batorch.utils.misc import compute_stats_regression
    prob_levels = np.linspace(start=0.05, stop=0.95, num=19)
    
    datamodule: DataModule = hydra.utils.instantiate(config.datamodule)
    # datamodule.setup("test")
    sd_noise=datamodule.test_dataset.sig_noise
    x_test=datamodule.test_dataset.x.cpu().detach().numpy().squeeze()
    y_test=datamodule.test_dataset.y.cpu().detach().numpy().squeeze()
    y_true=datamodule.test_dataset.y_true_mean.cpu().detach().numpy().squeeze()
    list_statistics = []
    for prob_level in prob_levels: 
        statistics = compute_stats_regression(probability_level_ci=prob_level, sd_noise=sd_noise, x_test=x_test, y_test=y_test, y_true=y_true, predictions=return_dict["test_preds"])  
        re = statistics["ratio_in_epistemic"]
        ra = statistics["ratio_in_aleatoric"]
        logger.info(f"Prob. level: {prob_level}, Ratio Epistemic: {re}, Ratio Aleatoric: {ra}")
        list_statistics.append(statistics)
    np.save(os.path.join(os.getcwd(),"statistics.npy"), list_statistics)
    
    logger.info(f"Log folder: {os.getcwd()}")
    
@hydra.main(config_path="configs/", config_name="config_mcdropout.yaml")
def mcdropout_pipeline(config: DictConfig):
    """Pipeline for Bayesian regression Monte Carlo dropout.
    
    The input configuration file is parsed by hydra. The input configuration file is parsed by hydra. The functions :func:`~batorch.utils.pipelines.train_loop` and :func:`batorch.utils.pipelines.test_loop` are used to perform Monte Carlo dropout.

    :param config: Configuration given by a :class:`omegaconf.DictConfig`.
    """    
    
    from batorch.utils.pipelines import train_loop, test_loop
    
    return_dict = {}
    return_dict["state_dicts"] = []
    
    if config.gpu:
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")
    
    if "seed" in config:
        torch.manual_seed(config.seed)

    logger.info(f"Instantiating neg. log likelihood <{config.neglikelihood._target_}>")                                           
    loglike: torch.nn.Module = hydra.utils.instantiate(config.neglikelihood)

    logger.info(f"Instantiating datamodule <{config.datamodule._target_}>")
    datamodule: DataModule = hydra.utils.instantiate(config.datamodule)
    #datamodule.setup()
    
    logger.info(f"Instantiating network <{config.network._target_}>")
    model: torch.nn.Module = hydra.utils.instantiate(config.network).to(device)
    num_params=0
    for p in model.parameters():
        num_params += p.numel()
    logger.info(f"The model has {num_params} parameters.")

    logger.info(f"Instantiating optimizer <{config.optimizer._target_}>")
    optimizer: Optimizer = hydra.utils.instantiate(config.optimizer, params=model.parameters())

    if config.lr_scheduler is not None:
        logger.info(f"Instantiating learning rate scheduler <{config.lr_scheduler._target_}>")
        scheduler: torch.optim._LRScheduler = hydra.utils.instantiate(config.lr_scheduler, optimizer=optimizer)
    else:
        scheduler = None
    
    writer = SummaryWriter(log_dir=HydraConfig.get().run.dir)
    
    output = train_loop(num_epoch=config.num_epochs, model=model,
                        train_loader=datamodule.train_dataloader(), val_loader=datamodule.val_dataloader(),
                        optimizer=optimizer, scheduler=scheduler, loglike=loglike,
                        writer=writer, device=device)
    
    return_dict["state_dicts"].append(model.state_dict())
    
    if "test_dataset" in config.datamodule and config.datamodule.test_dataset is not None:
        logger.info(f"Predicting the test data")
        predictions = []
        for _ in range(config.number_of_predictions):
            output = test_loop(model=model, 
                               test_loader=datamodule.test_dataloader(), 
                               loglike=loglike, 
                               writer=writer,
                               device=device,
                               enable_dropout=True)
            predictions.append(output["preds"])
        predictions = np.stack(predictions,axis=0)
        return_dict["test_preds"] = predictions          
    else:
        logger.info(f"No test dataset available.")
 
    writer.flush()
    
    if "save_state_dict" in config and config.save_state_dict:
        for i, state_dict in enumerate(return_dict["state_dicts"]):
            torch.save(state_dict, os.path.join(os.getcwd(),"map_state_dict_" + str(i) + ".pt"))
    if "save_test_preds" in config and config.save_test_preds:
        if "test_preds" in return_dict:
            np.save(os.path.join(os.getcwd(),"test_predictions.npy"), return_dict["test_preds"])

    logger.info(f"[6] Computing statistics")
    from batorch.utils.misc import compute_stats_regression
    prob_levels = np.linspace(start=0.05, stop=0.95, num=19)
    
    datamodule: DataModule = hydra.utils.instantiate(config.datamodule)
    
    sd_noise=datamodule.test_dataset.sig_noise
    x_test=datamodule.test_dataset.x.cpu().detach().numpy().squeeze()
    y_test=datamodule.test_dataset.y.cpu().detach().numpy().squeeze()
    y_true=datamodule.test_dataset.y_true_mean.cpu().detach().numpy().squeeze()
    list_statistics = []
    for prob_level in prob_levels: 
        statistics = compute_stats_regression(probability_level_ci=prob_level, sd_noise=sd_noise, x_test=x_test, y_test=y_test, y_true=y_true, predictions=return_dict["test_preds"])  
        re = statistics["ratio_in_epistemic"]
        ra = statistics["ratio_in_aleatoric"]
        logger.info(f"Prob. level: {prob_level}, Ratio Epistemic: {re}, Ratio Aleatoric: {ra}")
        list_statistics.append(statistics)
    np.save(os.path.join(os.getcwd(),"statistics.npy"), list_statistics)    