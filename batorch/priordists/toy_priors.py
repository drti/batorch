import torch
import torch.distributions as td

import logging
log = logging.getLogger(__name__)

from typing import Union, List
class GMMPrior():

    def __init__(self, var1=10.0, var2=1.0):
        self.var1 = torch.tensor(10,dtype=torch.float)
        self.sig1 = torch.sqrt(self.var1)
        self.var2 = torch.tensor(1,dtype=torch.float)
        self.sig2 = torch.sqrt(self.var2)        
        self.priorDistn1 = td.Normal(torch.tensor(0.0, dtype=torch.float), self.sig1)
        self.priorDistn2 = td.Normal(torch.tensor(0.0, dtype=torch.float), self.sig2)

    def to(self, device: Union[int, str], *args: List[str], non_blocking: bool = False):
        self.sig1 = self.sig1.to(device)
        self.sig2 = self.sig2.to(device)
        self.priorDistn1 = td.Normal(torch.tensor(0.0, dtype=torch.float), self.sig1)
        self.priorDistn2 = td.Normal(torch.tensor(0.0, dtype=torch.float), self.sig2)
        return self
    
    def log_dist(self, params):
        param = next(params)
        return self.priorDistn1.log_prob(param[0]) + self.priorDistn2.log_prob(param[1])
    
    def __call__(self, params):
        return -self.log_dist(params)
    
class MGMMPrior():
    
    def __init__(self):
        self.loc = torch.tensor([0,0], dtype=torch.float)
        self.cov = torch.tensor([[10, 0],[0, 10]],dtype=torch.float)
        self.priorDistn = td.MultivariateNormal(loc=self.loc, covariance_matrix=self.cov)
        
    def to(self, device: Union[int, str], *args: List[str], non_blocking: bool = False):
        self.loc = self.loc.to(device)
        self.cov = self.cov.to(device)
        self.priorDistn = td.MultivariateNormal(loc=self.loc, covariance_matrix=self.cov)
        return self

    def log_dist(self, params):
        param1 = next(params)
        param2 = next(params)
        return self.priorDistn.log_prob(param1.squeeze()) + self.priorDistn.log_prob(param2.squeeze())

    def __call__(self, params):
        return -self.log_dist(params)
    
class BananaPrior():
    
    def __init__(self):
        self.loc = torch.tensor([0.0, 0.0], dtype=torch.float)
        self.cov = torch.tensor([[0.1, 0.0], [0.0, 0.1]], dtype=torch.float)
        self.priorDistn = td.MultivariateNormal(loc=self.loc, covariance_matrix=self.cov)
    
    def to(self, device: Union[int, str], *args: List[str], non_blocking: bool = False):
        self.loc = self.loc.to(device)
        self.cov = self.cov.to(device)
        self.priorDistn = td.MultivariateNormal(loc=self.loc, covariance_matrix=self.cov)
        return self    
    
    def log_dist(self, params):
        param = next(params)
        return self.priorDistn.log_prob(param)
        
    def __call__(self, params):
        return -self.log_dist(params)