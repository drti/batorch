import torch.distributions as td

import logging
log = logging.getLogger(__name__)

from typing import Union, List
class Normal(td.Normal):

    def __init__(
        self,
        loc: float=0.0,
        scale: float=1.0,
        validate_args=None
    ):
        super().__init__(loc,scale,validate_args)

    def to(self, device: Union[int, str], *args: List[str], non_blocking: bool = False):
        self.loc = self.loc.to(device)
        self.scale = self.scale.to(device)
        return self

    def log_dist(self, params):
        lmbda=0
        for p in params:
            lmbda += self.log_prob(p).sum()
        return lmbda
    
    def __call__(self, params):
        return -self.log_dist(params)