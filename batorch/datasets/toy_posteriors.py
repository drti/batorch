import os
import torch
import numpy as np
from torch.utils.data import Dataset
from batorch.utils.misc import GMM, MultivariateGMM

class GaussianMixtureDataset(Dataset):

    def __init__(
        self,
        seed: int=0,
        samples_idx: list=[0,100]
        ):

        super().__init__()
        if type(seed)!=int or seed<0:
            raise TypeError("seed should be an integer greater than 0")
        if len(samples_idx)!=2:
            raise ValueError("samples_idx should of length 2, example: samples_idx = [0, 100]")
        if samples_idx[1]>100:
            raise ValueError("there are only 100 samples maximum in this dataset, samples_idx[1]<=100")
        gmm_data = self._generate_data(seed)
        self.sidx = samples_idx
        self.x = gmm_data[self.sidx[0]:self.sidx[1]]
        self.output_dim = 1

    def _generate_data(self, seed):
        np.random.seed(seed)
        mean = [0,1]
        sigma = [np.sqrt(2.0), np.sqrt(2.0)]
        cats = [0.5, 0.5]
        x = GMM(cats, mean, sigma, 100)
        return torch.tensor(x, dtype=torch.float32).unsqueeze(1)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return self.x[idx]

class MultivariateGaussianMixtureDataset(Dataset):

    def __init__(
        self,
        seed: int=0,
        samples_idx: list=[0,1000]
        ):

        super().__init__()
        if type(seed)!=int or seed<0:
            raise TypeError("seed should be an integer greater than 0")
        if len(samples_idx)!=2:
            raise ValueError("samples_idx should of length 2, example: samples_idx = [0, 100]")
        if samples_idx[1]>1000:
            raise ValueError("there are only 1000 samples maximum in this dataset, samples_idx[1]<=100")
        mgmm_data = self._generate_data(seed)
        self.sidx = samples_idx
        self.x = mgmm_data[self.sidx[0]:self.sidx[1]]
        self.output_dim = 1

    def _generate_data(self, seed):
        np.random.seed(seed)
        mean = [[0,0], [0.5,0.5]]
        sigma = [[1,0],[0,1]]
        cats = [0.5,0.5]
        x = MultivariateGMM(cats, mean, sigma, 1000)
        return torch.tensor(x, dtype=torch.float32)
    
    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return self.x[idx]

class SimpleBananaDataset(Dataset):
    
    def __init__(
        self,
        seed: int=0,
        samples_idx: list=[0,800]
        ):

        super().__init__()
        if type(seed)!=int or seed<0:
            raise TypeError("seed should be an integer greater than 0")
        if len(samples_idx)!=2:
            raise ValueError("samples_idx should of length 2, example: samples_idx = [0, 100]")
        if samples_idx[1]>1000:
            raise ValueError("there are only 1000 samples maximum in this dataset, samples_idx[1]<=100")
        banana_data = self._generate_data(seed)
        self.sidx = samples_idx
        self.x = banana_data[self.sidx[0]:self.sidx[1]]
        self.output_dim = 1.0

    def _generate_data(self, seed):
        np.random.seed(seed)
        theta = np.array([0.5, 0.0])
        mean = theta[0] + theta[1]**2
        sig = 3.0
        x = np.random.normal(loc=mean, scale=sig, size=800)
        return torch.tensor(x, dtype=torch.float32).unsqueeze(1)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return self.x[idx]