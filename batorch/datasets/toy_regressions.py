import os
import torch
import numpy as np
from torch.utils.data import Dataset

class HomoscedasticRegressionDataset(Dataset):

    def __init__(
        self,
        seed: int=0,
        num_samples: int=200,
        samples_idx: list=[0,100],
        bounds: list=[-3,3],
        sig_noise: float=0.3,
        ):

        super().__init__()
        self.sig_noise = sig_noise
        self.num_samples = num_samples
        self.lb, self.ub = bounds
        x, y = self._generate_data(seed)
        if type(seed)!=int or seed<0:
            raise TypeError("seed should be an integer greater than 0")
        if samples_idx==None:
            samples_idx=[0,num_samples]
        if len(samples_idx)!=2:
            raise ValueError("samples_idx should of length 2, example: samples_idx = [0, 100]")
        elif samples_idx[1]>200:
            raise ValueError("there are only {} samples maximum in this dataset, samples_idx[1]<={}".format(num_samples, num_samples))
        
        self.sidx = samples_idx
        self.x = x[self.sidx[0]:self.sidx[1]]
        self.y = y[self.sidx[0]:self.sidx[1]]
        self.y_true_mean = torch.cos(2.0*self.x) + torch.sin(self.x)
        self.output_dim = 1

    def _generate_data(self, seed):

        torch.manual_seed(seed)
        x = (self.ub-self.lb)*torch.rand(self.num_samples, dtype=torch.float32) + self.lb
        #x, _ = torch.sort(r)
        y = torch.cos(2.0*x) + torch.sin(x) + torch.randn(*x.shape, dtype=torch.float32)*self.sig_noise

        x = x.view((-1,1))
        y = y.view((-1,1))
        return x, y

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]

class HeteroscedasticRegressionDataset(Dataset):

    def __init__(
        self,
        seed: int=0,
        num_samples: int=200,
        samples_idx: list=[0,100],
        bounds: list=[-3,3],
        sig_noise: float=0.2,
        ):

        super().__init__()
        self.sig_noise = sig_noise
        self.num_samples = num_samples
        self.lb, self.ub = bounds
        x, y = self._generate_data(seed)
        if type(seed)!=int or seed<0:
            raise TypeError("seed should be an integer greater than 0")
        if samples_idx==None:
            samples_idx=[0,num_samples]
        if len(samples_idx)!=2:
            raise ValueError("samples_idx should of length 2, example: samples_idx = [0, 100]")
        if samples_idx[1]>200:
            raise ValueError("there are only {} samples maximum in this dataset, samples_idx[1]<={}".format(num_samples, num_samples))
        
        self.sidx = samples_idx
        self.x = x[self.sidx[0]:self.sidx[1]]
        self.y = y[self.sidx[0]:self.sidx[1]]
        self.y_true_mean = torch.cos(2.0*self.x) + torch.sin(self.x)
        self.output_dim = 1

    def _generate_data(self, seed):
        torch.manual_seed(seed)
        x = (self.ub-self.lb)*torch.rand(self.num_samples, dtype=torch.float32) + self.lb
        #x, _ = torch.sort(r)
        y = torch.cos(2.0*x) + torch.sin(x) + torch.randn(*x.shape, dtype=torch.float32)*self.sig_noise*(x + 2)
        x = x.view(-1,1)
        y = y.view(-1,1)
        return x, y

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]

class MismatchedRegressionDataset(Dataset):

    def __init__(
        self,
        seed: int=0,
        num_samples: int=200,
        samples_idx: list=[0,100],
        sig_noise: float=0.3,
        stage: str=None,
        ):

        super().__init__()
        self.sig_noise = sig_noise
        self.num_samples = num_samples
        x, y = self._generate_data(seed, stage)
        if type(seed)!=int or seed<0:
            raise TypeError("seed should be an integer greater than 0")
        if samples_idx==None:
            samples_idx=[0,num_samples]    
        if len(samples_idx)!=2:
            raise ValueError("samples_idx should of length 2, example: samples_idx = [0, 100]")
        if samples_idx[1]>200:
            raise ValueError("there are only {} samples maximum in this dataset, samples_idx[1]<={}".format(num_samples, num_samples))
        
        self.sidx = samples_idx
        self.x = x[self.sidx[0]:self.sidx[1]]
        self.y = y[self.sidx[0]:self.sidx[1]]
        self.y_true_mean = 0.1*self.x**3
        self.output_dim = 1

    def _generate_data(self, seed, stage):
        a, b, c, d = -4, -1, 1, 4
        prob = np.array([b-a, d-c])
        prob = prob/prob.sum()
        np.random.seed(seed)
        if stage=="train":
            x = np.array([np.random.choice([np.random.uniform(a, b), np.random.uniform(c, d)], 
                        p=prob)
                        for _ in range(self.num_samples)])
            epsilon = np.random.normal(loc=0.0, scale=self.sig_noise, size=self.num_samples)
            y = 0.1*x**3 + epsilon
        if stage=="test" or stage=="val":
            x = np.random.uniform(a, d, size=self.num_samples)
            #x = np.sort(x)
            epsilon = np.random.normal(loc=0.0, scale=self.sig_noise, size=self.num_samples)
            y = 0.1*x**3 + epsilon
        return torch.tensor(x, dtype=torch.float32).view(-1,1), torch.tensor(y, dtype=torch.float32).view(-1,1)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]

class MatchedRegressionDataset(Dataset):

    def __init__(
        self,
        seed: int=0,
        sig_noise: float=0.3,
        stage: str=None,
        ):

        super().__init__()
        self.sig_noise = sig_noise
        x, y = self._generate_data(seed, stage)
        if type(seed)!=int or seed<0:
            raise TypeError("seed should be an integer greater than 0")
            
        self.x = x
        self.y = y
        self.y_true_mean = -(1.0+self.x)*np.sin(1.2*self.x)
        self.output_dim = 1

    def _generate_data(self, seed, stage):
        np.random.seed(seed)
        a, b, c, d = -6, -2, 2, 6
        prob = np.array([b-a, d-c])
        prob = prob/prob.sum()
        if stage=="train":
            x_train1 = np.array([np.random.choice([np.random.uniform(a, b), np.random.uniform(c, d)], 
              p=prob)
            for _ in range(80)])
            x_train2 = np.random.uniform(-2, 2, size=2)
            x = np.hstack([x_train1, x_train2])
            epsilon = np.random.normal(loc=0.0, scale=self.sig_noise, size=82)
            y = -(1.0+x)*np.sin(1.2*x) + epsilon
        if stage=="test" or stage=="val":
            x = np.random.uniform(a, d, size=200)
            x = np.sort(x)
            epsilon = np.random.normal(loc=0.0, scale=self.sig_noise, size=200)
            y = -(1.0+x)*np.sin(1.2*x) + epsilon
        return torch.tensor(x, dtype=torch.float32).view(-1,1), torch.tensor(y, dtype=torch.float32).view(-1,1)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]

if __name__ == '__main__':
    pass