import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

class MlpNet(nn.Module):

    def __init__(self,
        input_dim: int,
        hidden_dim: int,
        output_dim: int,
        num_hidden_layers: int,
        activation: str = "relu",
        dropout_rate: float=0.0
        ):

        super(MlpNet, self).__init__()

        if activation=='relu':
            activation_func = nn.ReLU()      
        elif activation=='tanh':
            activation_func = nn.Tanh()
        elif activation=='sigmoid':
            activation_func = nn.Sigmoid()
        elif activation=='softmax':
            activation_func = nn.Softmax(dim=1)
        else:
            raise ValueError("trust.models.modules.mlp_net: activation function should be relu, tanh, sigmoid or softmax")

        mlp_modules = []
        mlp_modules.append(nn.Linear(input_dim, hidden_dim, bias=True))
        mlp_modules.append(activation_func)
        for _ in range(num_hidden_layers):
            mlp_modules.append(nn.Linear(hidden_dim, hidden_dim, bias=True))
            if dropout_rate>0.0:
                mlp_modules.append(nn.Dropout(p=dropout_rate))
            mlp_modules.append(activation_func)
        mlp_modules.append(nn.Linear(hidden_dim, output_dim, bias=True))
        self.mlp = nn.Sequential(*mlp_modules)

    def forward(self, x):
        return self.mlp(x)

if __name__ == "__main__":

    net1 = MlpNet(1, 50, 1, 0)
    num1 = 0
    for p in net1.parameters():
        num1 += p.numel()
    print(f"Mlp has {num1} parameters")