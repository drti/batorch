import numpy as np
import torch

class CyclicalScheduler(torch.optim.lr_scheduler.LambdaLR):

    def __init__(
        self,
        optimizer: torch.optim.Optimizer,
        lr_min: float=0.0001,
        lr_max: float=0.001,
        max_step: int=1e3,
        **kwargs,
        ):

        if lr_min<=0.0 or lr_max<=0.0:
            raise ValueError("lr_min and lr_max must be strictly positive")

        # lambda_fun = lambda epoch: lr_max*(1.0-(epoch % max_step)/max_step) + lr_min*(epoch % max_step)/max_step
        lambda_fun = lambda epoch: lr_max + ((epoch%max_step)/max_step)*(lr_min-lr_max)

        super(CyclicalScheduler, self).__init__(optimizer, lambda_fun)