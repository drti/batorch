import numpy as np
import torch

class PowerScheduler(torch.optim.lr_scheduler.LambdaLR):

    def __init__(
        self,
        optimizer: torch.optim.Optimizer,
        a: float=1000,
        b: float=0.33,
        ):

        if a <=0.0:
            raise ValueError("a must be strictly positive")

        lambda_fun = lambda epoch: np.power(1.0 + epoch/a, -b)

        super(PowerScheduler, self).__init__(optimizer, lambda_fun)