import os
import torch
import numpy as np

import hydra
from omegaconf import OmegaConf

import argparse
from pathlib import Path

from tqdm import tqdm

from batorch.priordists.normal import Normal

from batorch.rkhs.discrepancies import KSD, SlicedKSD
from batorch.rkhs.kernels import ImqKernel
from batorch.utils.misc import parameters_to_vector, vector_to_parameters
from batorch.utils.misc import uniform_hypersphere as randh

import logging
logger = logging.getLogger(__name__)

def get_weights_files(folder: str):
    weights = sorted([path for path in Path(folder).rglob("quantization_weights_samples.npy")] + [path for path in Path(folder).rglob("weights_samples.npy")])
    return weights

def estimate_lengthscale(filenames: list, m: int, n: int, device: torch.device):
    # Estimate \ell by subsampling all the samples
    idx = np.random.choice(m*len(filenames), n)
    bins = list(range(0,m*len(filenames)+m,m))

    cats = np.digitize(idx, bins)
    samples = []
    for i, cat in zip(idx, cats):
        f = cat-1
        j = i-(cat-1)*m
        data = np.load(filenames[f])
        samples.append(data[j])
    samples = np.stack(samples,axis=0)
    samples = torch.Tensor(samples)

    samples = samples.to(device)
    med_ksd = torch.median(torch.nn.functional.pdist(samples, p=2))
    # linv_ksd = 1.0/med
    
    g = randh(1000,samples.size(1)).to(device)
    projected_samples = torch.sum(samples * g, dim=1) # [N_samples]
    med_sksd = torch.median(torch.nn.functional.pdist(projected_samples.unsqueeze(1), p=2))
    # linv_sksd = 1.0/med
    
    del samples
    return med_ksd, med_sksd

def compute_ksds(algorithm: str, learning_rate: str, srcdir: str, dstdir: str, device: torch.device, r: torch.tensor, g: torch.tensor, med_ksd, med_sksd, 
                 loglikelihood, prior, train_dataset_cfg, val_dataset_cfg, test_dataset_cfg):
            
    dirname = os.path.join(srcdir, algorithm, "lr_" + learning_rate)
    weights = get_weights_files(dirname)
    num_files = len(weights)
    
    print(f"Directory: {dirname}")
    print(f"Number of weights files: {num_files}")
    print(f"Example of file: {weights[0]}")
    
    kernel_ksd = ImqKernel(lengthscale=med_ksd)
    kernel_sksd = ImqKernel(lengthscale=med_sksd)
    
    if len(weights)>0:
        
        output_path = os.path.join(dstdir, algorithm, "lr_" + learning_rate)
        if not os.path.isdir(output_path):
            os.makedirs(output_path)
                    
        ksds = np.zeros(num_files)
        sksds = np.zeros(num_files)
        for j, weight_file in tqdm(enumerate(weights)):
            
            weight_dir = os.path.dirname(weight_file)
            print(f"{weight_dir}")
            print(f"Seed: ", int(weight_dir.split("/")[-1]))
            train_dataset_cfg["seed"] = int(weight_dir.split("/")[-1])
            config = OmegaConf.create({"_target_": "batorch.utils.data.DataModule", "_recursive_": True, 
                      "train_dataset": train_dataset_cfg, "val_dataset": val_dataset_cfg, "test_dataset": test_dataset_cfg,
                      "kfold_splits": 1, "batch_size": 32, "num_workers": 0, "pin_memory": False})
            datamodule = hydra.utils.instantiate(config)
            
            # samples = torch.tensor(np.load(weight_file)).to(device)            
            samples = np.load(weight_file)
            samples = torch.from_numpy(samples.astype(np.float64))
            samples = samples.to(device)
            
            grads = torch.zeros_like(samples)
            for i in range(len(samples)):
                vector_to_parameters(samples[i], loglikelihood.parameters(), grad=False)
                loglikelihood.zero_grad()
                for batch_idx, (x, y) in enumerate(datamodule.train_dataloader()):
                    x, y = x.to(device), y.to(device)
                    negloglike = loglikelihood(x, y)
                    if batch_idx==0:
                        neglogprior = prior(loglikelihood.parameters())
                        neglogpost = negloglike + neglogprior
                    else:
                        neglogpost = negloglike
                    neglogpost.backward()
                grads[i] = parameters_to_vector(loglikelihood.parameters(), grad=True, both=False, cpu=False)
            grads.mul_(-1.0)                    
            
            
            ksd = KSD(x=samples, grad_x=grads, kernel=kernel_ksd, stats="V", method="direct").detach().cpu().numpy()
            ksds[j] = ksd
            
            sksd = SlicedKSD(x=samples, grad_x=grads, kernel=kernel_sksd, r=r, g=g)
            sksds[j] = torch.mean(sksd).detach().cpu().numpy()
        
        np.save(os.path.join(output_path, "ksds-float64.npy"), ksds)
        np.save(os.path.join(output_path, "sksds_float64.npy"), sksds)

def compute_coverages(algorithm: str, learning_rates: dict, srcdir: str, dstdir: str):
    
    for learning_rate in learning_rates[algorithm]:

        dirname = os.path.join(srcdir, algorithm, "lr_" + learning_rate)
        files = sorted([path for path in Path(dirname).rglob("statistics.npy")])
        
        if len(files)>0:
        
            output_path = os.path.join(dstdir, algorithm, "lr_" + learning_rate)
            if not os.path.isdir(output_path):
                os.makedirs(output_path)
                        
            # Get test_dataset
            dirseed = os.path.dirname(files[0])
            data_list = np.load(files[0], allow_pickle=True)
            num_coverages = len(data_list)
            
            config = OmegaConf.load(os.path.join(dirseed,".hydra/config.yaml"))
            test_dataset = hydra.utils.instantiate(config.datamodule.test_dataset)
            y = test_dataset.y_true_mean.cpu().detach().numpy().squeeze()

            coverages = np.zeros((num_coverages,len(y)))
            for j in range(num_coverages):
                qhigh = np.zeros((len(files),len(y)))
                qlow = np.zeros((len(files),len(y)))
                
                for i, file in enumerate(files):
                    data = np.load(file, allow_pickle=True)[j]
                    qlow[i] = data["q1_epistemic"]
                    qhigh[i] = data["q2_epistemic"]
                    
                test = ((y-qhigh<=0) & (y-qlow>=0)).astype(int)
                coverage = np.mean(test, axis=0)
                coverages[j] = coverage            
            
            np.save(os.path.join(output_path, "coverages.npy"), coverages)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    # parser.add_argument("--srcdir", type=str, required=True, help="Folder where all the experiments are stored for a given test case")
    # parser.add_argument("--dstdir", type=str, required=True, help="Folder where the results (lengthscale, coverages, discrepancies) should be stored")    
    parser.add_argument("--experiment", type=str, default="HomoscedasticRegression")
    parser.add_argument("--estimate_lengthscale", action="store_true")
    parser.add_argument("--estimate_discrepancies", action="store_true")
    parser.add_argument("--estimate_coverages", action="store_true")
    parser.add_argument("--learning_rate_id", type=int, default=0) # only useful if estimate_discrepancies=True
    parser.add_argument("--algorithm", type=str, default="SGLD") # only useful if estimate_discrepancies=True
    parser.add_argument("--num_projs", type=int, default=1000) # only useful if estimate_discrepancies=True
    parser.add_argument("--num_params", type=int, default=10401) # only useful if estimate_discrepancies=True
    args = parser.parse_args()
    experiment=args.experiment

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if experiment=="HomoscedasticRegression":
        modeldict = {"_target_": "batorch.networks.mlp_net.MlpNet",
                "input_dim": 1, "hidden_dim": 100, "output_dim": 1,
                "num_hidden_layers": 1, "activation": "relu"}
        config = OmegaConf.create({"_target_": "batorch.likelihoods.regression_negloglike.HomoscedasticGaussianNLL",
                                   "model": modeldict, "variance_prior_dist": None, "variance_prior": 0.09, "learn_variance": False})
        loglikelihood = hydra.utils.instantiate(config).to(device)
        prior = Normal(loc=0.0, scale=1.0)
        train_dataset_cfg = {"_target_": "batorch.datasets.toy_regressions.HomoscedasticRegressionDataset", 
                             "seed": 1, "num_samples": 100, "samples_idx": [0,100], "bounds": [-3,3], "sig_noise": 0.3}
        val_dataset_cfg = {"_target_": "batorch.datasets.toy_regressions.HomoscedasticRegressionDataset", 
                             "seed": 1, "num_samples": 200, "samples_idx": [100,200], "bounds": [-3,3], "sig_noise": 0.3}
        test_dataset_cfg = {"_target_": "batorch.datasets.toy_regressions.HomoscedasticRegressionDataset", 
                            "seed": 0, "num_samples": 200, "samples_idx": [0,100], "bounds": [-5,5], "sig_noise": 0.3}
    elif experiment=="MismatchedRegression":
        modeldict = {"_target_": "batorch.networks.mlp_net.MlpNet",
                "input_dim": 1, "hidden_dim": 50, "output_dim": 1,
                "num_hidden_layers": 2, "activation": "relu"}
        config = OmegaConf.create({"_target_": "batorch.likelihoods.regression_negloglike.HomoscedasticGaussianNLL",
                                   "model": modeldict, "variance_prior_dist": None, "variance_prior": 0.25, "learn_variance": False})
        loglikelihood = hydra.utils.instantiate(config).to(device)
        prior = Normal(loc=0.0, scale=1.0)
        train_dataset_cfg = {"_target_": "batorch.datasets.toy_regressions.MismatchedRegressionDataset", 
                             "seed": 1, "stage": "train", "num_samples": 200, "samples_idx": [0,200], "sig_noise": 0.5}
        val_dataset_cfg = train_dataset_cfg
        test_dataset_cfg = {"_target_": "batorch.datasets.toy_regressions.MismatchedRegressionDataset", 
                             "seed": 0, "stage": "test", "num_samples": 200, "samples_idx": [0,200], "sig_noise": 0.5}
    elif experiment=="MatchedRegression":
        modeldict = {"_target_": "batorch.networks.mlp_net.MlpNet",
                "input_dim": 1, "hidden_dim": 50, "output_dim": 1,
                "num_hidden_layers": 2, "activation": "relu"}
        config = OmegaConf.create({"_target_": "batorch.likelihoods.regression_negloglike.HomoscedasticGaussianNLL",
                                   "model": modeldict, "variance_prior_dist": None, "variance_prior": 0.25, "learn_variance": False})
        loglikelihood = hydra.utils.instantiate(config).to(device)
        prior = Normal(loc=0.0, scale=1.0)
        train_dataset_cfg = {"_target_": "batorch.datasets.toy_regressions.MatchedRegressionDataset", 
                             "seed": 1, "stage": "train", "sig_noise": 0.5}
        val_dataset_cfg = train_dataset_cfg
        test_dataset_cfg = {"_target_": "batorch.datasets.toy_regressions.MatchedRegressionDataset", 
                             "seed": 0, "stage": "test", "sig_noise": 0.5}        
    else:
        raise("Experiment not configured yet")

    cccwork = os.getenv("CCCWORKDIR")
    cccscratch = os.getenv("CCCSCRATCHDIR")
    srcdir = os.path.join(cccscratch, "neurips2022/regression_problems", experiment, "coverage/batch_size_32")
    dstdir = os.path.join(cccwork, "neurips2022/regression_problems", experiment, "coverage/batch_size_32")

    algorithms = ["ensemble", "mcdropout", "pSGLD", "SGHMCSA", "SGLD", "SGLDCV", "SGLDSVRG", "simpleSGHMC", "simpleSGHMCCV", "simpleSGHMCSVRG"]
    learning_rates = {"ensemble": [str(lr) for lr in np.logspace(-5,-1,10).tolist()],
                    "mcdropout": [str(lr) for lr in np.logspace(-5,-1,10).tolist()],
                    "pSGLD": [str(lr) for lr in np.logspace(-3,-1,10).tolist()],
                    "SGHMCSA": [str(lr) for lr in np.logspace(-5,-2,10).tolist()],
                    "SGLD": [str(lr) for lr in np.logspace(-7,-4,10).tolist()],
                    "SGLDCV": [str(lr) for lr in np.logspace(-7,-4,10).tolist()],
                    "SGLDSVRG": [str(lr) for lr in np.logspace(-7,-4,10).tolist()],
                    "simpleSGHMC": [str(lr) for lr in np.logspace(-7,-4,10).tolist()],
                    "simpleSGHMCCV": [str(lr) for lr in np.logspace(-7,-4,10).tolist()],
                    "simpleSGHMCSVRG": [str(lr) for lr in np.logspace(-7,-4,10).tolist()],
    }
    
    if args.estimate_lengthscale:
        
        logger.info("Estimating lengthscales")
        
        weights = sorted([path for path in Path(srcdir).rglob("quantization_weights_samples.npy")] + [path for path in Path(experiment).rglob("weights_samples.npy")])
        logger.info(f"Number of weights files: {len(weights)} and number of samples {2000*len(weights)}")
        med_ksd, med_sksd = estimate_lengthscale(filenames=weights, m=2000, n=1000, device=device)
        
        med = np.array([med_ksd.detach().cpu().numpy().item(), med_sksd.detach().cpu().numpy().item()])
        np.save(os.path.join(dstdir, "med.npy"), med)
        
        logger.info(f"Estimated lengthscales: {med}")
        
    elif args.estimate_discrepancies:
        
        logger.info("Loading lengthscales")
        
        meds = np.load(os.path.join(dstdir, "med.npy"))
        med_ksd = torch.tensor(meds[0]).to(device)
        med_sksd = torch.tensor(meds[1]).to(device)

        logger.info(f"Loaded lenghtscales: {meds}")
        logger.info("Estimating discrepancies")
        
        torch.manual_seed(12345)
        r = randh(args.num_projs,args.num_params).double().to(device)
        g = randh(args.num_projs,args.num_params).double().to(device)
        learning_rate = learning_rates[args.algorithm][args.learning_rate_id]
        compute_ksds(args.algorithm, learning_rate, srcdir, dstdir, device, r, g, med_ksd, med_sksd, loglikelihood, prior, train_dataset_cfg, val_dataset_cfg, test_dataset_cfg)
        
        logger.info("Done")
        
    elif args.estimate_coverages:
        
        logger.info("Estimating coverages")
        for algorithm in algorithms:
            compute_coverages(algorithm, learning_rates, srcdir, dstdir)
        logger.info("Done")