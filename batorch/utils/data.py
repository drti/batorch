import torch
from omegaconf import DictConfig
from typing import List, Union
from torch.utils.data import DataLoader, Dataset

from sklearn.model_selection import KFold

import logging
logger = logging.getLogger(__name__)
class DataModuleBase():

    def __init__(
        self,
        train_dataset: Union[DictConfig,Dataset],
        val_dataset: Union[DictConfig,Dataset,None],
        test_dataset: Union[DictConfig,Dataset,None],
        batch_size: int = 1,
        num_workers: int = 0,
        pin_memory: bool = False,
        **kwargs
    ):

        self.batch_size = batch_size
        self.num_workers = num_workers
        self.pin_memory = pin_memory

        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset

    def train_dataloader(self):
        raise NotImplementedError
    
    def val_dataloader(self):
        raise NotImplementedError

    def test_dataloader(self):
        raise NotImplementedError

    @property
    def get_num_train_data(self):
        raise NotImplementedError

    @property
    def get_batch_size(self):
        return self.batch_size
class DataModule(DataModuleBase):

    def __init__(
        self,
        train_dataset: Union[DictConfig,Dataset],
        val_dataset: Union[DictConfig,Dataset,None],
        test_dataset: Union[DictConfig,Dataset,None],
        batch_size: int = 1,
        num_workers: int = 0,
        pin_memory: bool = False,
        kfold_splits: int = 1,
        **kwargs
    ):
        super().__init__(train_dataset, val_dataset, test_dataset, batch_size, num_workers, pin_memory, **kwargs)
        self.kfold_splits = kfold_splits

    def kfold_train_val_dataloaders(self):
        kf = KFold(n_splits=self.kfold_splits)
        def dataloaders(kfold):
            for train_idx, val_idx in kfold.split(self.train_dataset.x):
                train_dataset_fold =  torch.utils.data.Subset(self.train_dataset, train_idx)
                val_dataset_fold =  torch.utils.data.Subset(self.train_dataset, val_idx)
                train_loader_fold = DataLoader(dataset=train_dataset_fold, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory, shuffle=True, collate_fn=collate, drop_last=True)
                val_loader_fold = DataLoader(dataset=val_dataset_fold, batch_size=self.batch_size, num_workers=self.num_workers, pin_memory=self.pin_memory, shuffle=False, collate_fn=collate, drop_last=True)
                yield train_loader_fold, val_loader_fold
        return dataloaders(kf)

    def train_dataloader(self):
        return DataLoader(
            dataset=self.train_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            shuffle=True,
            drop_last=True
        )
    
    def val_dataloader(self):
        if self.val_dataset is not None:
            return DataLoader(
                dataset=self.val_dataset,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                pin_memory=self.pin_memory,
                shuffle=False,
                drop_last=True
            )
        else:
            return None

    def test_dataloader(self):
        return DataLoader(
            dataset=self.test_dataset,
            batch_size=self.batch_size,
            num_workers=self.num_workers,
            pin_memory=self.pin_memory,
            shuffle=False
        )

    @property
    def get_num_train_data(self):
        return len(self.train_dataset)