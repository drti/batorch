import os
import torch
import numpy as np

@torch.no_grad()
def parameters_to_vector(parameters, grad=False, both=False, cpu=False):
    """Convert parameters or/and their gradients to one vector
    
    Adapted from https://github.com/cics-nd/cnn-surrogate/blob/master/utils/misc.py
    
    :param parameters: an iterator of Variables that are the parameters of a model.
    :param grad:  Vectorizes gradients if true, otherwise vectorizes params both (bool): If True, vectorizes both parameters and their gradients, 
            `grad` has no effect in this case. Otherwise vectorizes parameters
            or gradients according to `grad`.
    :return: The parameters or/and their gradients (each) represented by a single vector (th.Tensor, not Variable).
    """
    if not both:
        vec = []
        if not grad:
            for param in parameters:
                vec.append(param.detach().clone().view(-1))
        else:
            for param in parameters:
                vec.append(param.grad.detach().clone().view(-1))
        if cpu:
            return torch.cat(vec).cpu()
        return torch.cat(vec)        
    else:
        vec_params, vec_grads = [], []
        for param in parameters:
            vec_params.append(param.detach().clone().view(-1))
            vec_grads.append(param.grad.detach().clone().view(-1))
        if cpu:
            return torch.cat(vec_params).cpu(), torch.cat(vec_grads).cpu()
        return torch.cat(vec_params), torch.cat(vec_grads)

@torch.no_grad()
def vector_to_parameters(vec, parameters, grad=True):
    """Convert one vector to the parameters or gradients of the parameters
    
    Adapted from https://github.com/cics-nd/cnn-surrogate/blob/master/utils/misc.py
    
    :param vec: a single vector represents the parameters of a model.
        parameters (Iterable[Variable]): an iterator of Variables that are the parameters of a model.
    :param grad: True for assigning de-vectorized `vec` to gradients.
    """
    pointer = 0
#    with torch.no_grad():
    if grad:
        for param in parameters:
            num_param = param.numel()
            param.grad.copy_(vec[pointer:pointer + num_param].view(param.size()))
            pointer += num_param
    else:
        for param in parameters:
            num_param = param.numel()
            param.copy_(vec[pointer:pointer + num_param].view(param.size()))
            pointer += num_param

def compute_stats_regression(probability_level_ci: float, sd_noise: float, x_test: np.ndarray, y_test: np.ndarray, y_true: np.ndarray, predictions: np.ndarray):
    """Function that computes quantiles and other statistics.

    :param probability_level_ci: Probability level of the confidence region.
    :type probability_level_ci: float
    :param sd_noise: Standard deviation of the Gaussian noise.
    :type sd_noise: float
    :param x_test: Input test data.
    :type x_test: np.ndarray
    :param y_test: Output test data.
    :type y_test: np.ndarray
    :param predictions: Predictions made by one or :math:`n` models.
    :type predictions: np.ndarray of shape :math:`[n, ...]`.
    :return: Dictionary with keys `probability_level_ci`, `sd_noise`, `q1_epistemic`, `q2_epistemic`, `q1_aleatoric`, `q2_aleatoric`, `median`, `ratio_in_epistemic`, `ratio_in_aleatoric`.
    :rtype: dict
    """
    quantile_lb = 0.5*(1.0 - probability_level_ci)
    quantile_ub = probability_level_ci + quantile_lb

    q1_epistemic = np.quantile(predictions, quantile_lb, axis=0).squeeze()
    q2_epistemic = np.quantile(predictions, quantile_ub, axis=0).squeeze()
    above_q2 = (y_true-q2_epistemic>0)
    under_q1 = (y_true-q1_epistemic<0)
    outs_ci_epistemic = above_q2.sum() + under_q1.sum()
    ratio_out_epistemic = outs_ci_epistemic/y_test.size
    ratio_in_epistemic = 1.0-ratio_out_epistemic
    
    aleatoric_predictions = predictions + np.random.normal(loc=0.0, scale=sd_noise, size=predictions.shape)
    q1_total = np.quantile(aleatoric_predictions, quantile_lb, axis=0).squeeze()
    q2_total = np.quantile(aleatoric_predictions, quantile_ub, axis=0).squeeze()
    above_q2_total = (y_test-q2_total>0)
    under_q1_total = (y_test-q1_total<0)
    outs_ci_aleatoric = above_q2_total.sum() + under_q1_total.sum()
    ratio_out_aleatoric = outs_ci_aleatoric/y_test.size
    ratio_in_aleatoric = 1.0-ratio_out_aleatoric

    median = np.median(predictions, axis=0).squeeze()        

    stats = dict(probability_level_ci=probability_level_ci, sd_noise=sd_noise, 
                 q1_epistemic=q1_epistemic, q2_epistemic=q2_epistemic, q1_aleatoric=q1_total, q2_aleatoric=q2_total,
                 median=median, ratio_in_epistemic=ratio_in_epistemic, ratio_in_aleatoric=ratio_in_aleatoric)
    return stats

def uniform_hypersphere(n_samples: int, dim: int):
    r"""Function that generates vectors on the hypersphere.
    Given :math:`N` realisations :math:`x(\theta_1), \dots, x(\theta_N)` of the a :math:`d`-dimensional Gaussian random variable :math:`X`,
    the samples on the hypersphere a given by
    
    .. math::
        y(\theta_i) = \frac{x(\theta_i)}{\|x(\theta_i)\|}\,, \quad i = 1,\dots,N\,.

    :param n_samples: Number of samples.
    :type n_samples: int
    :param dim: Dimension of the hypersphere.
    :type dim: int
    :return: Matrix of samples.
    :rtype: torch.Tensor
    """
    
    X = torch.randn(n_samples,dim)
    Y = X/torch.linalg.norm(X,dim=1).unsqueeze(1)
    return Y

def GMM(p_i, mu, sig, n):
    r"""
    One-dimensional Gaussian Mixture : 
    
                    p(x) = \sum_{J=1}^{len(p_i}} p_i[J] \mathcal{N}(x|mu[J],sig[J]**2)
    
    :param p_i: coefficients of the mixture
    :type p_i: list
    :param mu: matrix gathering the mean values of the individual multivariate Gaussians
    :type mu: np.array
    :param cov: covariance matrix shared by the individual multivariate Gaussians
    :type cov: np.array
    :param n: number of samples
    :type n: int
    :return: vector gathering n samples of the Gaussian Mixture
    :rtype: np.array
    """
    if len(p_i)!=len(mu) or len(p_i)!=len(sig) or len(mu)!=len(sig):
        raise ValueError('p_i, mu, and var must have the same lengths: len(p_i) = %4d, len(mu) = %4d, len(sig) = %4d' % (len(p_i), len(mu), len(sig)))

    x = []
    for i in range(n):
        z_i = np.argmax(np.random.multinomial(1, p_i))
        x_i = np.random.normal(mu[z_i], sig[z_i])
        x.append(x_i)
    return np.asarray(x)

def MultivariateGMM(p_i, mu, cov, n):
    r"""
    Multivariate Gaussian Mixture that share the same covariance matrix: 

                    p(x) = \sum_{J=1}^{len(p_i}} p_i[J] \mathcal{N}(x|mu[J],cov)

    :param p_i: coefficients of the mixture
    :type p_i: list
    :param mu: matrix gathering the mean values of the individual multivariate Gaussians
    :type mu: np.array
    :param cov: covariance matrix shared by the individual multivariate Gaussians
    :type cov: np.array
    :param n: number of samples
    :type n: int
    :return: vector gathering n samples of the Gaussian Mixture
    :rtype: np.array
    """
    x = []
    for i in range(n):
        z_i = np.argmax(np.random.multinomial(1, p_i))
        x_i = np.random.multivariate_normal(mu[z_i], cov, size=1).T
        x.append(x_i)
    return np.asarray(x).squeeze()
