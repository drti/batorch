import torch
import torch.nn as nn
import torch.distributions as td

import logging
from typing import Union

from omegaconf import DictConfig
log = logging.getLogger(__name__)

class HomoscedasticGaussianNLL(nn.Module):
    """Homoscedastic Gaussian negative log-likelihood.

    :param variance_prior: Prior value for the variance.
    :param variance_prior_dist: Prior distribution of the variance. If not none, a sample will be drawn from this distribution.
    :learn_variance: If true, the log-variance is defined as a learnable parameter.
    """

    def __init__(
        self,
        variance_prior: float,
        variance_prior_dist: Union[td.Distribution,DictConfig,None]=None,
        learn_variance: bool=False,
        eps: float=1.e-6
    ):

        super(HomoscedasticGaussianNLL, self).__init__()
        if variance_prior_dist is not None:
            self.prior = variance_prior_dist
            log_beta_prior = self.prior.sample()
        else:
            self.prior = None
            inverse_variance_prior = torch.tensor(1.0/(variance_prior + eps), dtype=torch.float32)
            log_beta_prior = torch.log(inverse_variance_prior)

        if learn_variance:
            self.register_parameter(name='log_beta', param=torch.nn.Parameter(log_beta_prior))
        else:
            self.log_beta = log_beta_prior
        self.loss = nn.GaussianNLLLoss(full=False, reduction="sum")
        
    def forward(self, input, target):
        input = input.contiguous()
        target = target.contiguous()

        variance = (torch.ones(len(target)).type_as(target)*1.0/self.log_beta.exp())
        logLike = self.loss(input, target, variance)
        if self.prior is not None:
            prior_value = -self.prior.log_prob(self.log_beta)
            logLike = logLike + prior_value
        return logLike

class HeteroscedasticGaussianNLL(nn.Module):

    def __init__(
        self,
        eps: float=1.e-6
    ):
        super(HeteroscedasticGaussianNLL, self).__init__()
        self.loss = nn.GaussianNLLLoss(full=False, reduction="sum")

    def forward(self, input, target):
        input = input
        target = target
        pred = input[:,0:-1].contiguous()
        log_variance = input[:,-1:].expand_as(pred).contiguous()
        logLike = self.loss(pred, target, log_variance.exp())
        return logLike