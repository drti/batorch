import torch
import torch.nn as nn

import logging
from typing import Union

from omegaconf import DictConfig
log = logging.getLogger(__name__)

class GaussianNLL(torch.nn.Module):

    def __init__(
        self,
        model,
        variance_prior,
        eps: float=1.e-6
    ):

        super().__init__()
        self.model = model            
        inverse_variance_prior = torch.tensor(1.0/(variance_prior + eps), dtype=torch.float32)
        self.log_beta = torch.log(inverse_variance_prior)
        self.loss = torch.nn.GaussianNLLLoss(full=False, reduction="sum")
        
    def forward(self, x, y):
        y_pred = self.model(x)
        y_true = y.reshape(y_pred.size())
        
        variance = (torch.ones(len(y_true)).type_as(y_true)*1.0/self.log_beta.exp())
        logLike = self.loss(y_pred, y_true, variance)
        return logLike
class HomoscedasticGaussianNLL(nn.Module):
    """Homoscedastic Gaussian negative log-likelihood for regression problems.

    :param model: A surrogate model given by a torch.nn.module.
    :param variance_prior: Prior value for the variance.
    :param variance_prior_dist: Prior distribution of the variance. If not none, a sample will be drawn from this distribution.
    :learn_variance: If true, the log-variance is defined as a learnable parameter.
    """

    def __init__(
        self,
        model: Union[DictConfig, nn.Module],
        variance_prior: float,
        variance_prior_dist: DictConfig=None,
        learn_variance: bool=False,
        eps: float=1.e-6
    ):

        super(HomoscedasticGaussianNLL, self).__init__()
        self.model = model
        if variance_prior_dist is not None:
            self.prior = variance_prior_dist
            log_beta_prior = self.prior.sample()
        else:
            self.prior = None
            inverse_variance_prior = torch.tensor(1.0/(variance_prior + eps), dtype=torch.float32)
            log_beta_prior = torch.log(inverse_variance_prior)

        if learn_variance:
            self.register_parameter(name='log_beta', param=torch.nn.Parameter(log_beta_prior))
        else:
            self.log_beta = log_beta_prior
        self.loss = nn.GaussianNLLLoss(full=False, reduction="sum")
        
    def forward(self, x, y):
        y_pred = self.model(x)
        y_true = y.reshape(y_pred.size())
        
        variance = (torch.ones(len(y_true)).type_as(y_true)*1.0/self.log_beta.exp())
        logLike = self.loss(y_pred, y_true, variance)
        if self.prior is not None:
            prior_value = -self.prior.log_prob(self.log_beta)
            logLike = logLike + prior_value
        return logLike