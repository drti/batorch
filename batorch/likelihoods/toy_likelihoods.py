import torch
import torch.nn as nn
import torch.distributions as td

import logging
log = logging.getLogger(__name__)

class GMMLikelihood(nn.Module):

    def __init__(self):
        super(GMMLikelihood, self).__init__()
        self.var1 = torch.tensor(10,dtype=torch.float)
        self.sig1 = torch.sqrt(self.var1)
        self.var2 = torch.tensor(1,dtype=torch.float)
        self.sig2 = torch.sqrt(self.var2)
        self.var_x = torch.tensor([2, 2], dtype=torch.float)
        self.sig_x = torch.sqrt(self.var_x)
        self.register_parameter(name='theta1', param=nn.Parameter( torch.normal(mean=torch.zeros(2), std=torch.stack([self.sig1, self.sig2]) )))

    def forward(self, x):
        scale = self.sig_x.to(x.device)
        probs = td.Categorical(torch.tensor([0.5,0.5]).to(x.device))
        distn = td.MixtureSameFamily(
            probs, 
            td.Normal(torch.stack((self.theta1[0], self.theta1.sum())), scale)
            )
        return -distn.log_prob(x).sum()
    
class MGMMLikelihood(nn.Module):

    def __init__(self):
        super(MGMMLikelihood, self).__init__()
        std = torch.sqrt(torch.tensor(10.0, dtype=torch.float32))
        self.register_parameter(name='theta1', param=nn.Parameter( torch.concat([torch.randn(1)*std, torch.randn(1)*std]) ))
        self.register_parameter(name='theta2', param=nn.Parameter( torch.concat([torch.randn(1)*std, torch.randn(1)*std]) ))
                
    def forward(self, x):
        probs = td.Categorical(torch.tensor([0.5,0.5]).to(x.device))
        distn = td.MixtureSameFamily(
            probs, 
            td.MultivariateNormal(loc=torch.stack((self.theta1,self.theta2),axis=0), covariance_matrix=torch.eye(2).to(x.device))
            )
        return -distn.log_prob(x).sum()
    
class BananaLikelihood(nn.Module):

    def __init__(self,):
        super(BananaLikelihood, self).__init__()
        self.register_parameter(name='theta1', param=nn.Parameter( torch.normal(mean=torch.zeros(2, dtype=torch.float32), std=torch.tensor([0.1, 0.1], dtype=torch.float32))) )
        
    def forward(self, x):
        distn = td.Normal(loc=self.theta1[0]+self.theta1[1]**2, scale=torch.tensor(3.0, dtype=torch.float).to(x.device))
        return -distn.log_prob(x).sum()