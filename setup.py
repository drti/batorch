from setuptools import setup
  
setup(
    name="batorch",
    version="0.1",
    packages=["batorch"],
    description="A package that implements a few Bayesian approaches for neural networks",
    author="Safran Group",
    author_email="",
    entry_points={"console_scripts": ["batorch_map = hydra_entries:map_pipeline", 
                                      "batorch_mcmc = hydra_entries:mcmc_pipeline",  
                                      "batorch_ensemble = hydra_entries:ensemble_pipeline", 
                                      "batorch_mcdropout = hydra_entries:mcdropout_pipeline"]},
    include_package_data=True
)