.. BaTorch documentation master file, created by
   sphinx-quickstart on Thu Dec  9 09:28:21 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BaTorch's documentation!
===================================

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Package Reference

   modules/sgmcmc
   modules/rkhs
   modules/hydrapip

.. toctree::
   :maxdepth: 1
   :caption: Tutorials

   notes/bnn