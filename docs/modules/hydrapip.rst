Hydra pipelines
===============

Pipelines and utils
-------------------

.. currentmodule:: batorch.utils.pipelines
.. autosummary::
    :nosignatures:

    batorch.utils.pipelines.mcmc
    batorch.utils.pipelines.train_loop
    batorch.utils.pipelines.test_loop

.. automodule:: batorch.utils.pipelines
    :members:
    :undoc-members:
    :exclude-members: r2_scores, activate_dropout, deactivate_dropout

Entry points 
------------

.. automodule:: hydra_entries
    :members:
    :undoc-members: