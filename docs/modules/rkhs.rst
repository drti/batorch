batorch.rkhs
============

batorch.rkhs.discrepancies
--------------------------

.. currentmodule:: batorch.rkhs.discrepancies
.. autosummary::
    :nosignatures:

    batorch.rkhs.discrepancies.MMD
    batorch.rkhs.discrepancies.KSD
    batorch.rkhs.discrepancies.SlicedKSD
  
.. automodule:: batorch.rkhs.discrepancies
    :members:
    :undoc-members:

batorch.rkhs.kernels
--------------------------

.. currentmodule:: batorch.rkhs.kernels
.. autosummary::
    :nosignatures:

    batorch.rkhs.kernels.Kernel
    batorch.rkhs.kernels.ImqKernel
    batorch.rkhs.kernels.EnergyKernel
  
.. automodule:: batorch.rkhs.kernels
    :members:
    :undoc-members:
    
batorch.rkhs.quantization
--------------------------

.. currentmodule:: batorch.rkhs.quantization
.. autosummary::
    :nosignatures:

    batorch.rkhs.quantization.Quantization
    batorch.rkhs.quantization.QuantizationMMD
      
.. automodule:: batorch.rkhs.quantization
    :members:
    :undoc-members: